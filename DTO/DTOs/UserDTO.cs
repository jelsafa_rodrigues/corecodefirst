﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO.Dtos
{
    public class UserDTO : BaseDTO
    {
        public String Name { get; set; }
        public String Password { get; set; }
        public String ConfirmPassword { get; set; }
        public String Email { get; set; }
        public String Token { get; set; }
    }
}
