﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;
using Domain;
using Microsoft.Extensions.Logging;

namespace DomainModel
{
    public class BaseDomain
    {
        protected MySqlBaseContext context;
        protected ILogger logger;

        public BaseDomain(MySqlBaseContext context, ILoggerFactory loggerFactory)
        {
            this.context = context;
            this.logger = loggerFactory.CreateLogger("MySqlBaseContext");
        }
    }
}
