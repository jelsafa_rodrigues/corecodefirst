﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Model
{
    public class User : BaseModel
    {
        public String Name { get; set; }
        public String Email { get; set; }
        public String Token { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public bool IsEmailValidated { get; set; } = false;
    }
}
