import { Component, Inject, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { User } from '../users/user/user.class';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})
export class LoginComponent {

    public user: User;
    public baseUrl: any;
    
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, private router: Router, private route: ActivatedRoute) {
        this.baseUrl = baseUrl;
        this.user = new User();
    }

    onSignIn() {
        this.http.post(this.baseUrl + 'api/login/authenticate', this.user).subscribe(result => {
            //this.user = result.json() as User;
            console.log(result.json());
        }, error => console.error(error));
    }
}
