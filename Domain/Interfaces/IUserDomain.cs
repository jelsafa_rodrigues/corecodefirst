﻿using DomainModel.Model;
using DTO.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface IUserDomain
    {
        void AddUpdate(UserDTO user);

        UserDTO Get(long Id);

        List<UserDTO> GetAll();

        UserDTO Authenticate(UserDTO userDTO);
    }
}
