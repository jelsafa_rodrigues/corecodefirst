import { Component, Inject, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { User } from './user.class';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'user',
    templateUrl: './user.component.html'
})
export class UserComponent {

    public user: User;
    public id: any;
    public baseUrl: any;
    
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, private router: Router, private route: ActivatedRoute) {
        this.route.params.subscribe(params => { this.id = params.id });//console.log(params) });
        this.baseUrl = baseUrl;

        if (this.id == 0) {
            this.user = new User();
        } else {
            http.get(baseUrl + 'api/user/' + this.id).subscribe(result => {
                this.user = result.json() as User;
            }, error => console.error(error));
        }
    }

    onSave() {
        this.http.post(this.baseUrl + 'api/user', this.user).subscribe(result => {
            this.user = result.json() as User;
        }, error => console.error(error));
    }
}
