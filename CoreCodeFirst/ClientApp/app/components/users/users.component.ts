import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { User } from './user/user.class';
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: 'users',
    templateUrl: './users.component.html'
})
export class UsersComponent {
    public users: User[];

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, private router: Router) {
        http.get(baseUrl + 'api/user').subscribe(result => {
            this.users = result.json() as User[];
        }, error => console.error(error));
    }

    onGet(id: number) {
        this.router.navigate(['users', id]);
    }
}
