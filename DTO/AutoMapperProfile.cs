﻿using DomainModel.Model;
using DTO.Dtos;
using AutoMapper;

namespace DTO
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserDTO, User>();
            CreateMap<User, UserDTO>();
        }
    }
}
