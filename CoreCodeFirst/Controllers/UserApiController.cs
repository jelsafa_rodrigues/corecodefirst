﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DomainModel.Model;
using Domain.Domains;
using Domain.Interfaces;
using Domain;
using DTO.Dtos;

namespace CoreCodeFirst.Controllers
{
    [Produces("application/json")]
    [Route("api/user")]
    public class UserApiController : BaseApi
    {
        private IUserDomain userDomain;

        public UserApiController(IUserDomain userDomain)
        {
            this.userDomain = userDomain;
        }

        [HttpPost]
        public ObjectResult Post([FromBody] UserDTO user)
        {
            try
            {
                userDomain.AddUpdate(user);
                return Ok("User inserted successully");
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        public ObjectResult Get(long id)
        {
            return Ok(userDomain.Get(id));
        }

        [HttpGet]
        public ObjectResult GetAll()
        {
            return Ok(userDomain.GetAll());
        }

        [HttpPost("[action]")]
        public ObjectResult Authenticate([FromBody] UserDTO user)
        {
            return Ok(userDomain.GetAll());
        }
    }
}