﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainModel
{
    public class BaseModel
    {
        [Key]
        public long Id { get; set; }
    }
}
