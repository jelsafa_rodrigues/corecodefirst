﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using DomainModel.Model;
using Domain.Interfaces;
using System.Linq;
using DTO.Dtos;
using AutoMapper;
using Common.Helpers;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.Extensions.PlatformAbstractions;

namespace Domain.Domains
{
    public class UserDomain : IUserDomain
    {
        protected MySqlBaseContext context;
        protected ILogger logger;
        private IMapper mapper;
        
        public UserDomain(MySqlBaseContext context, ILoggerFactory loggerFactory, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public void AddUpdate(UserDTO userDTO)
        {
            User user = mapper.Map<User>(userDTO);
            if (user.Id == 0)
            {
                // validation
                if (string.IsNullOrWhiteSpace(userDTO.Password))
                    throw new AppException("Password is required");

                // equals password and confirm password
                if (!userDTO.Password.Equals(userDTO.ConfirmPassword))
                    throw new AppException("Password is required");

                if (this.context.User.Any(x => x.Name == userDTO.Name))
                    throw new AppException("Username " + userDTO.Name + " is already taken");

                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(userDTO.Password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                CreateToken(user, out JwtSecurityToken token);

                user.Token = new JwtSecurityTokenHandler().WriteToken(token);

                this.context.User.Add(user);
            }
            else
            {
                if (!this.context.User.Any(r => r.Id == userDTO.Id))
                    throw new AppException("User not found");

                if (userDTO.Name != user.Name)
                {
                    // username has changed so check if the new username is already taken
                    if (context.User.Any(x => x.Name == userDTO.Name))
                        throw new AppException("Username " + userDTO.Name + " is already taken");
                }

                // update user properties
                user.Name = userDTO.Name;

                // update password if it was entered
                if (!string.IsNullOrWhiteSpace(userDTO.Password))
                {
                    byte[] passwordHash, passwordSalt;
                    CreatePasswordHash(userDTO.Password, out passwordHash, out passwordSalt);

                    user.PasswordHash = passwordHash;
                    user.PasswordSalt = passwordSalt;
                }

                CreateToken(user, out JwtSecurityToken token);

                user.Token = new JwtSecurityTokenHandler().WriteToken(token);

                this.context.User.Update(user);
            }
            this.context.SaveChanges();
        }

        public UserDTO Authenticate(UserDTO userDTO)
        {
            if (string.IsNullOrEmpty(userDTO.Name) || string.IsNullOrEmpty(userDTO.Password))
                return null;

            var user = this.context.User.SingleOrDefault(x => x.Name == userDTO.Name);

            // check if username exists
            if (user == null)
                return null;

            // check if is the email validated
            if (!user.IsEmailValidated)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(userDTO.Password, user.PasswordHash, user.PasswordSalt))
                return null;

            // authentication successful
            return mapper.Map<UserDTO>(user);
        }

        public UserDTO Get(long Id)
        {
            UserDTO userDTO = mapper.Map<UserDTO>(context.User.FirstOrDefault(r => r.Id == Id));
            return userDTO;
        }

        public List<UserDTO> GetAll()
        {
            List<UserDTO> list = mapper.Map<List<UserDTO>>(context.User.ToList());
            return list;
        }

        private static void CreateToken(User user, out JwtSecurityToken token)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF32.GetBytes(user.Email));

            var claims = new Claim[] {
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(JwtRegisteredClaimNames.Email, user.Email)
            };

            token = new JwtSecurityToken(
                issuer: PlatformServices.Default.Application.ApplicationName,
                audience: "client",
                claims: claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddDays(28),
                signingCredentials: new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256)
            );
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
    }
}
