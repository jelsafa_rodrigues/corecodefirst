﻿using DomainModel.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace DomainModel
{
    public class MySqlBaseContext : DbContext
    {
        public MySqlBaseContext(DbContextOptions<MySqlBaseContext> options) : base(options)
        { }

        public DbSet<User> User { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>().HasKey(m => m.Id);

            // shadow properties
            builder.Entity<User>().Property<DateTime>("UpdatedTimestamp");

            base.OnModelCreating(builder);
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();

            updateUpdatedProperty<User>();

            return base.SaveChanges();
        }

        private void updateUpdatedProperty<T>() where T : class
        {
            var modifiedSourceInfo =
                ChangeTracker.Entries<T>()
                    .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified);

            foreach (var entry in modifiedSourceInfo)
            {
                entry.Property("UpdatedTimestamp").CurrentValue = DateTime.UtcNow;
            }
        }
    }
}
